# Trabajo de SIBD #

- Ignacio Dominguez Martínez-Casanueva

- Ricardo J. Ruiz Fernández

- Doris Ana Sângeap


## 0 - Estructura del código del repositorio ##

El repositorio se divide en los directorios:

 - code: Contiene el código Scala. También tiene la carpeta "files" donde se generaban los csvs y ficheros con los logs y los resultados; la carpeta "target" donde se generaba el .jar al utilizar Maven; y un script "script.sh" que utilizamos hasta el momento de introducir Kubernetes. También se encuentra el pom.xml con las dependencias del proyecto padre.

 - datasets: Contiene los 3 datasets, reducido el mayor a 10.000 líneas.

 - kubernetes: Contiene los ficheros de configuración en "config" y las imágenes creadas de Docker en "images".

 - visualization: Contiene los ficheros para ejecutar Zeppelin configurado para utilizar el puerto 8090, aunque solo pudimos realizarlo en local (sin Kubernetes). 


## 1 - Instalación ##

### Instalar Scala y Spark ###

https://www.tutorialspoint.com/apache_spark/apache_spark_installation.htm



### Downgrear Spark a la versión 2.0.0 si tienes una superior (para Zeppelin) ###

Copia de seguridad de spark como lo tenemos ya:

mv /usr/local/spark /usr/local/spark2 

Acceder a http://d3kbcqa49mib13.cloudfront.net/spark-2.0.0-bin-hadoop2.7.tgz

sudo tar xvf spark-2.0.0*

sudo mv spark-2.0.0... /usr/local/spark


### Instalar Hadoop ###

https://www.tutorialspoint.com/hadoop/hadoop_enviornment_setup.htm

Notas: Cuidado con los espacios en los .xml. Si da problemas de acceso -> chmod -R 777 en /hadoopinfra	


### Instalar Maven ###

sudo apt-get install maven



## 2 - Instalación y ejecución en local (sin Kubernetes) ##

Creamos la carpeta reco en Hadoop DFS:

hadoop fs -mkdir /home/NOMBREUSUARIO/hadoopinfra/hdfs/reco

Nota: Si da error hay que crear las carpetas una a una 

Ir a la carpeta datasets/:

hadoop fs -put artist_alias.txt artist_data.txt user_artist_data.txt hdfs://localhost:9000/home/NOMBREUSUARIO/hadoopinfra/hdfs/reco

Comprobamos con:

hadoop fs -ls /home/NOMBREUSUARIO/hadoopinfra/hdfs/reco


Ir al directorio code/:

mvn package

spark-submit --master local \target/code-recommender-2.0.0-jar-with-dependencies.jar <ID_USUARIO_PARA_EL_QUE_SE_HACE_LA_RECOMENDACION>

* Nota - Para quitar warnings: *
export HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=/usr/local/hadoop/lib/native"



### 2.1  Script de ejecución en local (sin Kubernetes) ###


Tiene dos campos: la carpeta $HOME (ej: /home/ricardo) y el nº de usuario (ej: 1000002)

El script está ahora mismo en code/. Lo que hace es:

 - rm -R target/: Borra target

 - mvn -q package Genera el nuevo target

 - spark-submit --master local \target/ch03-recommender-2.0.0-jar-with-
 dependencies.jar 1000002 > files/results.txt 2> files/logs.txt: Ejecuta lo del target, manda los logs (todos los INFO de Spark) a logs.txt y el resultado a results.txt.   NOTA: Esto es lo que suele dar problemas, en el script está comentado lo mismo pero sin mandar los logs a un fichero, para ver qué falla 

 - cat files/results.txt: Saca por pantalla lo de results.txt

 - mv /home/ricardo/sibd_rnd/ch03-recommender/files/song.csv/*.csv /home/ricardo/sibd_rnd/ch03-recommender/files/artist.csv: Como se generaba una carpeta con un csv con un nombre raro, se saca de la carpeta

 - rm -R /home/ricardo/sibd_rnd/ch03-recommender/files/song.csv/: Se borra de la carpeta



## 2.2 Visualización en local (sin Kubernetes) ##


Se realiza mediante Apache Zeppelin (https://zeppelin.apache.org/). La instalación se realizó mediante el siguiente vídeo: https://www.youtube.com/watch?v=4oiPmaBlJNA . No fuimos capaces de utilizar Zeppelin junto a Kubernetes, pero como demostramos en el examen sí que lo llegamos a utilizar a nivel local.

Para iniciar el servicio: 

~/sibd_rnd/visualization/zeppelin/bin/zeppelin-daemon.sh start


Acceder a localhost:8090. El código que hay que meter en los paragraphs (los recuadros) está en ~/sibd_rnd/visualization/paragraph1.txt y paragraph2.txt



## 3 - Instalación y ejecución utilizando Kubernetes ## 


# Despliegue de cluster en Kubernetes #

# Instalación de Minikube #

Se recomienda emplear VirtualBox ACTUALIZADO

https://github.com/kubernetes/minikube/releases

# Instalación de Kubernetes CLI #

https://kubernetes.io/docs/getting-started-guides/kubectl/

# Creación de minikube # 

```
#!bash

#Importante indicar máxima memoria posible (sin dejar vacÃ­o al host)
#Por ejemplo 6G sería: 
minikube start --memory=6000 

#Al finalizar ejecutar script de configuración de contexto kubernetes
#Colocarse en directorio kubernetes
./start-minikube

#Cargar demonio de docker en el shell
eval $(minikube docker-env

```
# Preparación de imágenes docker # 

```
#!bash

#En cada directorio dentro de images correspondiente ejecutar docker build:
docker build -t hadoop-sibd .
docker build -t spark-master-sibd .
docker build -t spark-worker-sibd .
docker build -t spark-driver-sibd .

```
# Creación de pods en kubernetes # 

```
#!bash

#Desde el directorio kubernetes/config:
kubectl create -f hadoop-controller.yaml
kubectl create -f hadoop-service.yaml
kubectl create -f spark-master-controller.yaml
kubectl create -f spark-master-service.yaml
kubectl create -f spark-worker-controller.yaml

# 1000002 id de usuario y 5 cantidad de artistas recomendados
kubectl run spark-driver --image=spark-driver-sibd --image-pull-policy=Never --restart=Never -- 1000002 5

```

# IMPORTANTE: Redirección de puertos al host #

Para poder acceder a los puertos de los servicios dentro del host como "localhost:puerto" activar redirección en kubernetes: 

```
#!bash

kubectl port-forward <nombre_del_pod> puertoEnHost:puertoServicio &

# Por ejemplo: Para habilitar el WebUI de Hadoop. Accederemos en el navegador web con localhost:50070
kubectl port-forward <nombre_del_pod_de_hadoop> 50070:50070 &

# Para obtener el nombre de cada pod 
kubectl get pods

```
