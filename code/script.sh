#!/bin/sh

echo "Borrando target ..."
rm -R target/ 

echo "Generando jar..."
mvn -q package

echo "Ejecutando programa ..."
spark-submit --master local \target/code-recommender-2.0.0-jar-with-dependencies.jar $1 $2 $3 $4 2> files/logs.txt

#mv $1/sibd_rnd/code/files/best.csv/*.csv $1/sibd_rnd/code/files/top10.csv
#mv $1/sibd_rnd/code/files/worst.csv/*.csv $1/sibd_rnd/code/files/bottom10.csv

#rm -R $1/sibd_rnd/code/files/best.csv/
#rm -R $1/sibd_rnd/code/files/worst.csv/

#python callSpotify.py
