/*
 * Copyright 2015 and onwards Sanford Ryza, Juliet Hougland, Uri Laserson, Sean Owen and Joshua Wills
 *
 * See LICENSE file for further information.
 */

package com.cloudera.datascience.recommender

import scala.collection.Map
import scala.collection.mutable.ArrayBuffer
import scala.util.Random
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.ml.recommendation.{ALS, ALSModel}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.log4j.{Level, Logger}

object RunRecommender {

  def main(args: Array[String]): Unit = {
    
    // Esto quita todos los logs
    /// val log = Logger.getRootLogger()
    /// log.setLevel(Level.ERROR)
    
    val spark = SparkSession.builder().getOrCreate()
    
    val base   = args(0) 
    val csv_path = args(1)
    val userId = args(2)
    val number = args(3)
    
    // val base = "hdfs://localhost:9000"+myHome+"/hadoopinfra/hdfs/reco/"

    val rawUserArtistData = spark.read.textFile(base + "user_artist_data.txt")
    val rawArtistData = spark.read.textFile(base + "artist_data.txt")
    val rawArtistAlias = spark.read.textFile(base + "artist_alias.txt")

    val runRecommender = new RunRecommender(spark)
    runRecommender.recommend(rawUserArtistData, rawArtistData, rawArtistAlias, userId, csv_path, number)

  }

}

class RunRecommender(private val spark: SparkSession) {

  import spark.implicits._

  
  def recommend(rawUserArtistData: Dataset[String],rawArtistData: Dataset[String],rawArtistAlias: Dataset[String],userId: String,csv_path: String,number: String): Unit = {

    val bArtistAlias = spark.sparkContext.broadcast(buildArtistAlias(rawArtistAlias))
    val allData = buildCounts(rawUserArtistData, bArtistAlias).cache()
    val model = new ALS().
      setSeed(Random.nextLong()).
      setImplicitPrefs(true).
      setRank(10).setRegParam(1.0).setAlpha(40.0).setMaxIter(20).
      setUserCol("user").setItemCol("artist").
      setRatingCol("count").setPredictionCol("prediction").
      fit(allData)
    allData.unpersist()


    calculateAndPrint("top","best", userId, number, model, rawArtistData,csv_path)
    // calculateAndPrint("bottom","worst", userId, number, model, rawArtistData,csv_path)

    model.userFactors.unpersist()
    model.itemFactors.unpersist()
    
    
  }

  def calculateAndPrint(caca: String, fileName: String, userId: String, number: String, model: ALSModel, rawArtistData: Dataset[String], csv_path: String) {


        val userID = userId.toInt
        val number_ = number.toInt

        val topRecommendations = makeRecommendations(model, userID, number_,caca)
        val recommendedArtistIDs = topRecommendations.select("artist").as[Int].collect()
        val artistByID = buildArtistByID(rawArtistData)
        val id_name = artistByID.join(spark.createDataset(recommendedArtistIDs).toDF("id"), "id").select("*")
        val merged = topRecommendations.join(id_name, topRecommendations("artist") === id_name("id") ).select("id","name","prediction")
        merged.show()
        merged.coalesce(1).write.option("header","true").format("com.databricks.spark.csv").save(csv_path)

  }

  def buildArtistByID(rawArtistData: Dataset[String]): DataFrame = {
    rawArtistData.flatMap { line =>
      val (id, name) = line.span(_ != '\t')
      if (name.isEmpty) {
        None
      } else {
        try {
          Some((id.toInt, name.trim))
        } catch {
          case _: NumberFormatException => None
        }
      }
    }.toDF("id", "name")
  }

  def buildArtistAlias(rawArtistAlias: Dataset[String]): Map[Int,Int] = {
    rawArtistAlias.flatMap { line =>
      val Array(artist, alias) = line.split('\t')
      if (artist.isEmpty) {
        None
      } else {
        Some((artist.toInt, alias.toInt))
      }
    }.collect().toMap
  }

  def buildCounts(
      rawUserArtistData: Dataset[String],
      bArtistAlias: Broadcast[Map[Int,Int]]): DataFrame = {
    rawUserArtistData.map { line =>
      val Array(userID, artistID, count) = line.split(' ').map(_.toInt)
      val finalArtistID = bArtistAlias.value.getOrElse(artistID, artistID)
      (userID, finalArtistID, count)
    }.toDF("user", "artist", "count")
  }

  def makeRecommendations(model: ALSModel, userID: Int, howMany: Int, topOrBottom:String): DataFrame = {
    
    val toRecommend = model.itemFactors.
      select($"id".as("artist")).
      withColumn("user", lit(userID))
    
    if(topOrBottom == "top"){
      model.transform(toRecommend).
        select("artist", "prediction").
        orderBy($"prediction".desc).
        limit(howMany)
    }else{
      model.transform(toRecommend).
          select("artist", "prediction").
          orderBy($"prediction".asc).
          limit(howMany)   
    }

  }


}