#!/bin/bash

hdfs dfs -copyToLocal hdfs://hadoop-storage:9000/user/root/results/driver_$1/top10.csv /root/

cd /root/ && python callSpotify.py

rm /root/top10.csv