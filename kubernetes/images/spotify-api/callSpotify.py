#!/usr/bin/env python

import os
import sys
import spotipy

sp = spotipy.Spotify()

def get_artist(name):
    results = sp.search(q='artist:' + name, type='artist')
    items = results['artists']['items']
    if len(items) > 0:
        return items[0]
    else:
        return None

def search_artist_top_tracks(artist):
    response = sp.artist_top_tracks(artist['id'])
    i = 0
   
    for track in response['tracks']:
        path = track['external_urls']['spotify']
        name = track['name']
        i = i + 1
        if (i < 10):
            print(str(i) + "  - " +name + " -> " + path)
        else:
            print(str(i) + " - " +name + " -> " + path)

def main():
    print "\r"
    file = open( 'top10.csv', 'r')
    content = file.read()
    names = content.split("\n")
    
    for line in names:
    #var hadoop_path = "hdfs://hadoop-storage:9000/user/root/results/driver_"+sys.argv[0]+"/top10.csv"
    #with hdfs.open(hadoop_path) as hadoopFile:
    #with hdfs.open('hdfs://hadoop-storage:9000/user/root/results/driver_XXX/fichero') as hadoopFile:
    #    for line in hadoopFile:
        if "," in line: 
            name = line.split(",")[1]
            if name!="name":
                print(" - Spotify "+str(name).upper()+"'s TOP 10 - ")
                print("")
                artist = get_artist(name)
                if artist:
                    search_artist_top_tracks(artist)
                else:
                    print "Can't find that artist", name

                print("")

# Start program
if __name__ == "__main__":
    main()
