#!/bin/bash

# $1 User ID
# $2 Amount of recommended artists

TIMESTAMP=$(date +%F_%H-%M-%S)
LOG_FILE=log_"$TIMESTAMP".txt
DRIVER_NAME=driver_"$1"

$SPARK_HOME/bin/spark-submit --master spark://spark-master:7077 \
	--class "com.cloudera.datascience.recommender.RunRecommender" \
	--driver-memory 4G --executor-memory 4G \
	jars/$TARGET_JAR $HADOOP_DATASETS $HADOOP_RESULTS/$DRIVER_NAME/csv $1 $2 >> $LOG_FILE

hdfs dfs -mv $HADOOP_RESULTS/$DRIVER_NAME/csv/*.csv $HADOOP_RESULTS/$DRIVER_NAME/top10.csv
hdfs dfs  -rm -r -f  $HADOOP_RESULTS/$DRIVER_NAME/csv

hdfs dfs -mkdir $HADOOP_RESULTS/$DRIVER_NAME/logs
hdfs dfs -put $LOG_FILE $HADOOP_RESULTS/$DRIVER_NAME/logs/